from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
candidate = Table('candidate', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('email', String(length=120)),
)

challenge = Table('challenge', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('description', String(length=255)),
    Column('result', String),
)

result = Table('result', post_meta,
    Column('candidate_id', Integer, primary_key=True, nullable=False),
    Column('challenge_id', Integer, primary_key=True, nullable=False),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['candidate'].create()
    post_meta.tables['challenge'].create()
    post_meta.tables['result'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['candidate'].drop()
    post_meta.tables['challenge'].drop()
    post_meta.tables['result'].drop()
