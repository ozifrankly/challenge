# -*- coding: utf-8 -*-

from flask import session
from flask.ext.wtf import Form
from wtforms import validators, BooleanField, TextField, TextAreaField, SubmitField, ValidationError
from wtforms.ext.sqlalchemy.validators import Unique
from models import Candidate, Challenge
from app import db

def get_session():
    return db.session


class CandidateForm(Form):
    name = TextField("Nome",[validators.required("Preencha nome")])
    email = TextField("Email",[validators.required("Preencha email"),
                               validators.Email("Email invalido"),
                               Unique(get_session,
                                      Candidate,
                                      Candidate.email,
                                      "Email ja cadastrado")])
    submit = SubmitField("Enviar")




class ChallengeForm(Form):
    name = TextField("Nome",[validators.required("Preencha nome")])
    description = TextAreaField("Descricao",[validators.required("Preencha descricao"),
                                   Unique(get_session,
                                          Challenge,
                                          Challenge.description,
                                          "Descrição ja usada")])
    result = TextField("Resultado",[validators.required("Preencha o resultado")])
    submit = SubmitField("Enviar")


class LoginForm(Form):
    openid = TextField('openid', [validators.required()])
    remember_me = BooleanField('remember_me', default = False)

