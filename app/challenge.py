# -*- coding: utf-8 -*-
import sys
from flask import render_template, session, request, redirect, g, url_for
from flask.ext.login import login_user, logout_user, current_user, login_required
from sqlalchemy import func 
from app.models import forms
from app.models.models import Result, Challenge, Candidate
from app import app, db, lm, oid

@lm.user_loader
def load_user(id):
    return Candidate.query.get(int(id))

@app.before_request
def before_request():
    g.user = current_user

                


@app.route('/',methods=['GET','POST'])
@login_required
def index():
    params={}
    params['user_id']=1
    candidate = Candidate.query.get(1)

    if request.method == "GET":
        last_result = Result.query.filter('candidate_id = %d' % 1)\
                                          .order_by('challenge_id desc')\
                                          .first()
        if last_result != None:
            params['challenge'] = Challenge.query.filter('id > %d' % \
                                            last_result.challenge_id).first()
        else:
            params['challenge'] = Challenge.query.first()

        return render_template('challenge.html', params = params)
    elif request.method == "POST":
        challenge = Challenge.query.get(request.form['challenge_id'])
        if compareResult(challenge.result,request.form['code']):
            result = Result(candidate_id = candidate.id, 
                            challenge_id = challenge.id)
            db.session.add(result)
            db.session.commit()
            return render_template('success.html',code=request.form['code'])
        else:
            last_result = Result.query.filter('candidate_id = %d' % 1)\
                                       .order_by('challenge_id desc')\
                                       .first()
            if last_result != None:
                params['challenge'] = Challenge.query.filter('id > %d' % \
                                              last_result.challenge_id).first()
            else:
                params['challenge'] = Challenge.query.first()
            return render_template('challenge.html',params=params)



def compareResult(expect,code):
    from cStringIO import StringIO
    old_stdout = sys.stdout
    redirected_output = sys.stdout = StringIO()
    exec(code)
    sys.stdout = old_stdout
    result = redirected_output.getvalue().rstrip('\n')
    return expect == result


@app.route('/cadastro/desafiante',methods=['GET','POST'])
def register_user():
    if request.method == "GET":
        form = forms.CandidateForm()
        return render_template('challenger_register.html', form = form)
    elif request.method == "POST":
        form = forms.CandidateForm(request.form)
        if form.validate_on_submit():
            candidate = Candidate(name = form.name.data,
                                         email = form.email.data)
            db.session.add(candidate)
            db.session.commit()
            return redirect(url_for("index"))
        else:
            return render_template('challenger_register.html', form = form)


@app.route('/cadastro/desafio',methods=['GET','POST'])
def register_challenge():
    if request.method == "GET":
        form = forms.ChallengeForm()
        return render_template('challenge_register.html', form = form)
    elif request.method == "POST":
        form = forms.ChallengeForm(request.form)
        if form.validate_on_submit():
            challenge = Challenge(name = form.name.data,
                                         description = form.description.data,
                                         result = form.result.data)
            db.session.add(challenge)
            db.session.commit()
            form = forms.ChallengeForm()
            return redirect(url_for("index"))
        else:
            return render_template('challenge_register.html', form = form)



@app.route('/login', methods = ['GET', 'POST'])
@oid.loginhandler
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = forms.LoginForm()
    if form.validate_on_submit():
        session['remember_me'] = form.remember_me.data
        candidate = Candidate.query.filter_by(name = form.openid.data).first()
        if candidate is None:
            return redirect('/cadastro/desafiante')
        login_user(candidate,remember = form.remember_me.data)
        return redirect(url_for("index"))
    return render_template('login.html', 
                           title = 'login',
                           form = form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))



if __name__ == '__main__':
    app.run()

