from sqlalchemy import Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from app import db

Base = declarative_base()



class Candidate(db.Model):
    __tablename__ = 'candidate'
    id = Column(Integer, primary_key = True)
    name = Column(String(64))
    email = Column(String(120))
    results = relationship("Result")

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)


    def __repr__(self):
        return '<candidate %r>' %self.name

class Challenge(db.Model):
    __tablename__ = 'challenge'
    id = Column(Integer, primary_key = True)
    name = Column(String(64), unique = True)
    description = Column(String(255), unique = True)
    result = Column(String)

    def __repr__(self):
        return '<candidate %r>' %self.name

class Result(db.Model):
    __tablename__ = 'result'
    candidate_id = Column(Integer, ForeignKey('candidate.id'), primary_key = True)
    challenge_id = Column(Integer, ForeignKey('challenge.id'), primary_key = True)


